import 'dart:io';
import 'dart:typed_data';

void main(List<String> args) async {
  final ip = InternetAddress.tryParse('0.0.0.0');
  final server = await ServerSocket.bind(ip, 42069);
  print('LISTENING ON ${server.address}:${server.port}');
  server.listen(handleClient);
}

void handleClient(Socket client) {
  print('got connection!');
  client.listen(
    (Uint8List data) {
      final command = String.fromCharCodes(data).trim().toLowerCase();
      print(command);
      if (command.contains('http')) {
        client.writeAll([
          'HTTP/1.1 001 OK\n\r',
          '\n\r',
          command.contains('curl')
              ? '\nCURL? Don\'t make me laugh...\n'
              : '\nDig deeper\n',
        ]);
        client.close();
      } else if (command == 'help') {
        final s1 = 'help - show this prompt';
        final s2 = 'dump - receive a hint dump';
        final s3 = 'exit - close this connection';
        final s4 = 'something else - that which you are looking for';
        client.writeAll([s1, s2, s3, s4], '\n');
      } else if (command == 'dump') {
        File('assets/bin').readAsBytes().then((bytes) {
          client.write(bytes);
        });
      } else if (command == 'leda') {
        File('assets/text').readAsBytes().then((bytes) {
          client.write(bytes);
        });
      } else if (command == 'exit') {
        client.close();
      }
    },
    onError: (Object error) {
      client.close();
    },
    onDone: () {
      client.close();
    },
  );
}
